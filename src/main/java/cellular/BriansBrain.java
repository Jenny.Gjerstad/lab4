package cellular;

public class BriansBrain extends GameOfLife{

    public BriansBrain(int rows, int columns) {
        super(rows, columns);
    }
    
    @Override
    public CellState getNextCell(int row, int col){
    	int aliveNeighbours = 0;
		try{
			aliveNeighbours = countNeighbors(row, col, CellState.ALIVE);
		}catch(IndexOutOfBoundsException e){
		}
		CellState nextCellState = CellState.DEAD;
		if(currentGeneration.get(row, col).equals(CellState.ALIVE)){
				nextCellState = CellState.DYING;
			}
        else if(currentGeneration.get(row, col).equals(CellState.DEAD)){
            if(aliveNeighbours == 2){
                nextCellState = CellState.ALIVE;
            }
            else{
                nextCellState = CellState.DEAD;
            }
        }
        else{
            nextCellState = CellState.DEAD;
        }
		return nextCellState;
	}
    
}
