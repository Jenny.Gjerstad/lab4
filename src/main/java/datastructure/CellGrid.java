package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    
    private int rows;
    private int columns;
    private CellState[][] state;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.columns = columns;
        this.state = new CellState[rows][columns];
        for(int i = 0; i<rows;i++){
            for(int j = 0; j<columns; j++){
                this.state[i][j] = initialState;
            }
        }
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if(row > this.rows || row < 0){
            throw new IndexOutOfBoundsException("Row need to be within: "+"0 and " + (this.rows-1));
        }
        if(column > this.columns || column < 0){
            throw new IndexOutOfBoundsException("Column need to be within: "+"0 and " + (this.columns-1));
        }
        this.state[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        if(row > this.rows || row < 0){
            throw new IndexOutOfBoundsException("Row need to be within: "+"0 and " + (this.rows-1));
        }
        if(column > this.columns || column < 0){
            throw new IndexOutOfBoundsException("Column need to be within: "+"0 and " + (this.columns-1));
        }
        return state[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newCellGrid = new CellGrid(this.rows, this.columns, CellState.DEAD);
        for(int i = 0; i < this.rows;i++){
            for(int j = 0; j < this.columns; j++){
                newCellGrid.set(i, j, this.state[i][j]);
            }
        }
        return newCellGrid;
    }
    
}
